import { useEffect, useState } from 'react'
import { useRouteMatch } from 'react-router-dom'

import ListTypeContainer from "../components/ListTypeContainer"
import NewsList from "../components/NewsList"

import './NewsPage.css';

import newsApi from '../apis/news'

const NewsPage = ({ newsProvider }) => {
  const { url } = useRouteMatch();

  const [news, setNews] = useState([]);

  const [categories, setCategories] = useState([]);

  const [currentCategory, setCurrentCategory] = useState("");

  useEffect(() => {
    fetchNews();
    fetchCategories();

  }, [url])

  const fetchNews = async () => {
    const res = await newsApi.get(`v1${url}`);

    setNews(res.data.data);
  }

  const fetchCategories = async () => {
    const res = await newsApi.get();

    setCategories(res.data.listApi[newsProvider].listType)
  }

  return (
    <div className="news-page container">
      <div className="row">
        <div className="col-9">
          <NewsList news={news} />
        </div>
        <div className="col-2">
          <ListTypeContainer categories={categories} setCurrentCategory={setCurrentCategory} />
        </div>
      </div>


    </div>
  )
}

export default NewsPage
