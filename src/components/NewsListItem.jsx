import { Card, CardBody, CardImg, CardTitle } from "reactstrap"

const NewsListItem = ({ title, image }) => {
  return (
    <>
      <Card className="col-4 my-1 px-0">
        {image && <CardImg top width="100%" src={`${image.small}`} alt="Card image cap" />}
        <CardBody>
          <CardTitle tag="h5">{title}</CardTitle>
          {/* <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle> */}
          {/* <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText> */}
          {/* <Button>Button</Button> */}
        </CardBody>
      </Card>
    </>
  )
}

export default NewsListItem
