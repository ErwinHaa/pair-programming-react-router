import { Route, Switch } from 'react-router-dom'

import Homepage from '../../pages/Homepage'
import NewsPage from '../../pages/NewsPage'

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Homepage />
      </Route>
      <Route exact path="/cnbc-news">
        <NewsPage newsProvider="CNBC News"/>
      </Route>
      <Route exact path="/republika-news">
        <NewsPage newsProvider="Republika News"/>
      </Route>
      <Route exact path="/tempo-news">
        <NewsPage newsProvider="Tempo News"/>
      </Route>
      <Route exact path="/okezone-news">
        <NewsPage newsProvider="Okezone News"/>
      </Route>
      <Route exact path="/bbc-news">
        <NewsPage newsProvider="BBC News" />
      </Route>
    </Switch>
  )
}

export default AppRoutes
