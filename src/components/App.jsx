import Navbar from "./Navbar"
import AppRoutes from "./routes/AppRoutes"

const App = () => {

  return (
    <div>
      <Navbar />
      <AppRoutes />
    </div>
  )
}

export default App
