import { NavLink } from 'react-router-dom'

const NavLinkMessage = ({ path, message, setCurrentCategory }) => {
  return <NavLink to={path} activeStyle={{
    fontWeight: "bold",
    color: "#3e4d4d"
  }} style={{ textDecoration: 'none', color: '#3e4d4d' }} >{message}</NavLink>
}

export default NavLinkMessage
