import NewsListItem from "./NewsListItem"

const NewsList = ({ news }) => {

  const renderedList = news.length && news.map(({ title, image }, i) => <NewsListItem key={i} title={title} image={image} />)
  console.log(renderedList);

  return (
    <div className="row justify-content-around">
      {renderedList}
    </div>
  )
}

export default NewsList
