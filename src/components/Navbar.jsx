import { Button, ButtonGroup } from 'reactstrap'
import NavLinkMessage from './NavLinkMessage'

const Navbar = () => {
  return (
    <nav className="text-center my-4">
      <Button color="secondary"><NavLinkMessage path="/cnbc-news" message="CNBC News" /></Button>{' '}
      <Button color="secondary"><NavLinkMessage path="/republika-news" message="Republika News" /></Button>{' '}
      <Button color="secondary"><NavLinkMessage path="/tempo-news" message="Tempo News" /></Button>{' '}
      <Button color="secondary"><NavLinkMessage path="/okezone-news" message="Okezone News" /></Button>{' '}
      <Button color="secondary"><NavLinkMessage path="/bbc-news" message="BBC News" /></Button>{' '}
      {/* <ul>
        <li><NavLinkMessage path="/cnbc-news" message="CNBC News" /></li>
        <li><NavLinkMessage path="/republika-news" message="Republika News" /></li>
        <li><NavLinkMessage path="/tempo-news" message="Tempo News" /></li>
        <li><NavLinkMessage path="/okezone-news" message="Okezone News" /></li>
        <li><NavLinkMessage path="/bbc-news" message="BBC News" /></li>
      </ul> */}
    </nav>
  )
}

export default Navbar
