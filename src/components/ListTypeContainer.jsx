import { useRouteMatch, useParams, Route, Switch } from 'react-router-dom'

import { List, Card, CardBody, ListGroup, ListGroupItem } from 'reactstrap';

import NewsPage from '../pages/NewsPage'

import NavLinkMessage from './NavLinkMessage'

const ListTypeContainer = ({ categories, setCurrentCategory }) => {

  // const { category } = useParams();

  const { url, path } = useRouteMatch();

  const renderedList = categories.map((category, i) => <ListGroupItem action tag="button"><NavLinkMessage key={i} path={`${url}/${category}`} message={category} setCurrentCategory={setCurrentCategory} /></ListGroupItem>)

  return (
    <aside>
      <ListGroup>
        <List type="unstyled" className="mx-3">
          {renderedList}
        </List>

        <Route path={`${path}/:category`}>
          <NewsPage />
        </Route>
      </ListGroup>
    </aside>
  )
}

export default ListTypeContainer
